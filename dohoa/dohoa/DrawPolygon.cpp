#include "DrawPolygon.h"
#include <iostream>
#include <math.h>
#include "Line.h"
using namespace std;

const double PI = 3.1415;


void DrawEquilateralTriangle(int xc, int yc, int R, SDL_Renderer *ren)
{
    int x[3];
    int y[3];
    float phi = PI/2 ;
    for (int i=0; i<3; i++)
    {
        x[i] = xc + (int)(R*cos(phi)+0.5); //
        y[i] = yc - (int)(R*sin(phi)+0.5);
        
        phi += 2*PI/3;
    }

    for (int i=0; i<3; i++)
    {
        Midpoint_Line(x[i], y[i], x[(i+1)%3], y[(i+1)%3], ren);
    }
}

void DrawSquare(int xc, int yc, int R, SDL_Renderer *ren)
{
    int x[4];
    int y[4];
    float phi = 0;
    for (int i=0; i<4; i++)
    {
        x[i] = xc + int(R*cos(phi)+0.5);
        y[i] = yc - int(R*sin(phi)+0.5);
        phi += 2*PI/4;
    }
    
    for (int i=0; i<4; i++)
    {
        Bresenham_Line(x[i], y[i], x[(i+1)%4], y[(i+1)%4], ren);
    }
}
void DrawPentagon(int xc, int yc, int R, SDL_Renderer *ren)
{
    int x[5];
    int y[5];
    float phi = PI/2;
    for (int i=0; i<5; i++)
    {
        x[i] = xc + int(R*cos(phi)+0.5);
        y[i] = yc - int(R*sin(phi)+0.5);
        phi += 2*PI/5;
    }
    
    for (int i=0; i<5; i++)
    {
        Bresenham_Line(x[i], y[i], x[(i+1)%5], y[(i+1)%5], ren);
    }
}
void DrawHexagon(int xc, int yc, int R, SDL_Renderer *ren)
{
    int x[6];
    int y[6];
    float phi = PI/2;
    for (int i=0; i<6; i++)
    {
        x[i] = xc + int(R*cos(phi)+0.5);
        y[i] = yc - int(R*sin(phi)+0.5);
        phi += 2*PI/6;
    }
    
    for (int i=0; i<6; i++)
    {
        Bresenham_Line(x[i], y[i], x[(i+1)%6], y[(i+1)%6], ren);
    }
}

void DrawStar(int xc, int yc, int R, SDL_Renderer *ren)
{
    int x[5];
    int y[5];
    float phi = PI/2;
    
    for (int i=0; i<5; i++)
    {
        x[i] = xc + int(R*cos(phi)+0.5);
        y[i] = yc - int(R*sin(phi)+0.5);
        phi += 2*PI/5;
    }
    
    for (int i = 0; i<5; i++)
    {
        Bresenham_Line(x[i], y[i], x[(i+2)%5], y[(i+2)%5], ren);
    }
}

void DrawEmptyStar(int xc, int yc, int R, SDL_Renderer *ren)
{
    int x[5], xp[5];
    int y[5], yp[5];
    float phi = PI/2;
    float rnho = R*sin(PI/10)/sin(7*PI/10);
    
    for (int i = 0; i < 5; i++)
    {
        x[i] = xc + int(R*cos(phi)+0.5);
        y[i] = yc - int(R*sin(phi)+0.5);
        xp[i] = xc + int(rnho*cos(phi+PI/5)+0.5);
        yp[i] = yc - int(rnho*sin(phi+PI/5)+0.5);
        phi += 2*PI/5;
    }
    
    for (int i = 0; i < 5; i++)
    {
        Bresenham_Line(x[i], y[i], xp[i], yp[i], ren);
        Bresenham_Line(xp[i], yp[i], x[(i+1)%5], y[(i+1)%5], ren);
    }
}

//Star with eight wings
void DrawStarEight(int xc, int yc, int R, SDL_Renderer *ren)
{
    int x[8], xp[8];
    int y[8], yp[8];
    float phi = 0;
    float rnho = R*sin(PI/8)/sin(6*PI/8);
    
    for (int i = 0; i < 8; i++)
    {
        x[i] = xc + int(R*cos(phi)+0.5);
        y[i] = yc - int(R*sin(phi)+0.5);
        xp[i] = xc + int(rnho*cos(phi+PI/8)+0.5);
        yp[i] = yc - int(rnho*sin(phi+PI/8)+0.5);
        phi += 2*PI/8;
    }
    
    for (int i = 0; i < 8; i++)
    {
        Bresenham_Line(x[i], y[i], xp[i], yp[i], ren);
        Bresenham_Line(xp[i], yp[i], x[(i+1)%8], y[(i+1)%8], ren);
    }
}

//For drawing one star of convergent star
void DrawStarAngle(int xc, int yc, int R, float startAngle, SDL_Renderer *ren)
{
    int x[5], xp[5];
    int y[5], yp[5];
    float phi = startAngle;
    float rnho = R*sin(PI/10)/sin(7*PI/10);
    
    for (int i = 0 ; i < 5; i++)
    {
        x[i] = xc + int(R*cos(phi)+0.5);
        y[i] = yc - int(R*sin(phi)+0.5);
        xp[i] = xc + int(rnho*cos(phi+PI/5)+0.5);
        yp[i] = yc - int(rnho*sin(phi+PI/5)+0.5);
        phi += 2*PI/5;
    }
    
    for (int i = 0; i < 5; i++)
    {
        Bresenham_Line(x[i], y[i], xp[i], yp[i], ren);
        Bresenham_Line(xp[i], yp[i], x[(i+1)%5], y[(i+1)%5], ren);
    }
}

void DrawConvergentStar(int xc, int yc, int r, SDL_Renderer *ren)
{
    float bk = r;
    float gbd = PI/2;
    while ( bk >1 )
    {
        DrawStarAngle(xc, yc, int(bk+0.5), gbd, ren);
        gbd = gbd + PI;
        bk = bk*sin(PI/10)/sin(7*PI/10);
    }
}

