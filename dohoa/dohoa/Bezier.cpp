#include "Bezier.h"
#include <iostream>
using namespace std;

void DrawCurve2(SDL_Renderer *ren, Vector2D p1, Vector2D p2, Vector2D p3)
{
    float x = 0, y = 0;
    for(float t = 0; t<=1; t+=0.0005)
    {
        x = (1-t)*(1-t)*p1.x + 2*(1-t)*t*p2.x + t*t*p3.x;
        y = (1-t)*(1-t)*p1.y + 2*(1-t)*t*p2.y + t*t*p3.y;
        SDL_RenderDrawPoint(ren, (int)x+0.5, (int)y+0.5);
    }
}
void DrawCurve3(SDL_Renderer *ren, Vector2D p1, Vector2D p2, Vector2D p3, Vector2D p4)
{
    float x = 0, y = 0;
    for(float t = 0; t<=1; t+=0.0005)
    {
        x = (1-t)*(1-t)*(1-t)*p1.x + 3*t*(1-t)*(1-t)*p2.x + 3*t*t*(1-t)*p3.x + t*t*t*p4.x;
        y = (1-t)*(1-t)*(1-t)*p1.y + 3*t*(1-t)*(1-t)*p2.y + 3*t*t*(1-t)*p3.y + t*t*t*p4.y;
        SDL_RenderDrawPoint(ren, (int)x+0.5, (int)y+0.5);
    }
}


