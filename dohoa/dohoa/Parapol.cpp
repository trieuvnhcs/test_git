#include "Parapol.h"

void Draw2Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
    int new_x;
    int new_y;
    
    new_x = xc + x;
    new_y = yc + y;
    SDL_RenderDrawPoint(ren, new_x, new_y);
    
    new_x = xc - x;
    new_y = yc + y;
    SDL_RenderDrawPoint(ren, new_x, new_y);
    
}
void BresenhamDrawParapolPositive(int xc, int yc, int A, SDL_Renderer *ren)
{
    //vung1
    // 0 <= x <= A
    // y >= 0
    //0 <= y' <= 1
    int x = 0, y = 0;
    int p;
    p = (x+1)*(x+1) -2*A*y - A;
    Draw2Points(xc, yc, x, y, ren );
    while( x <= A )
    {
        if(p <= 0 ){
            p += 2*x+3;
        }
        else{
            p += 2*x + 3 - 2*A;
            y++;
        }
        x++;
        Draw2Points(xc, yc, x, y, ren );
    }
    
    
    //vung2
    //x>= A
    // y>=0
    //y'>= 1
    x = A;
    y = A/2;
    p = -x*x - (x+1)*(x+1) + 4*A*(y+1);
    Draw2Points(xc, yc, x, y, ren );
    while( x <=  800 )
    {
        if(p <= 0 ){
            p += 4*A;
        }
        else{
            p += 4*A - 4*x - 4;
            x++;
        }
        y++;
        Draw2Points(xc, yc, x, y, ren );
    }
}

void BresenhamDrawParapolNegative(int xc, int yc, int A, SDL_Renderer *ren)
{
    //vung1
    int x = 0, y = 0;
    int p;
    p = (x+1)*(x+1) -2*A*y - A;
    Draw2Points(xc, yc, x, y, ren );
    while( x <= A )
    {
        if(p <= 0 ){
            p += 2*x+3;
        }
        else{
            p += 2*x + 3 - 2*A;
            y--;
        }
        x++;
        Draw2Points(xc, yc, x, y, ren );
    }
    
    //vung2
    x = A;
    y = A/2;
    p = -x*x - (x+1)*(x+1) + 4*A*(y+1);
    
    while( x <=  800 )
    {
        if(p <= 0 ){
            p += 4*A;
        }
        else{
            p += 4*A - 4*x - 4;
            x++;
        }
        y++;
        Draw2Points(xc, yc, x, -y, ren );
    }
}

