#ifndef CIRCLE_H
#define CIRCLE_H
#include <SDL2/SDL.h>
void BresenhamDrawCircle(int xc, int yc, int R, SDL_Renderer *ren);
void MidpointDrawCircle(int xc, int yc, int R, SDL_Renderer *ren);
void Draw8Points(int xc, int yc, int x, int y, SDL_Renderer *ren);
#endif

